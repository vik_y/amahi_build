git clone https://github.com/amahi/hda-platform
cd hda-platform
cat >.gitmodules <<EOL
[submodule "html"]
	path = html
	url = https://github.com/amahi/platform
	update = merge
EOL
git submodule init
git submodule update
cd html
git pull origin master
git checkout master
cd ..
make rpm
cp -R release/ ../release-platform/
